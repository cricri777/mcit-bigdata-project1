package ca.mcit.bigdata.project1.processor

import ca.mcit.bigdata.project1.model.Trips

import scala.collection.mutable.ListBuffer


trait TripsTypeProcessor {
  def process(filePath: String): List[Trips]
}

object TripsTypeProcessor {

  class Csv extends TripsTypeProcessor {
    /**
      * read csv file code inspired by : https://alvinalexander.com/scala/csv-file-how-to-process-open-read-parse-in-scalad
      * @param filePath
      * @return
      */
    override def process(filePath: String): List[Trips] = {
      var bufferedSource = io.Source.fromFile(filePath)

      var trips = new ListBuffer[Trips]()
      for (line <- bufferedSource.getLines.toStream) {
        var cols = line.split(",", -1)
        trips += new Trips(cols(0),cols(1),cols(2),cols(3),cols(4),cols(5),cols(6),cols(7),cols(8))
      }
      bufferedSource.close
      return trips.result()
    }
  }

}