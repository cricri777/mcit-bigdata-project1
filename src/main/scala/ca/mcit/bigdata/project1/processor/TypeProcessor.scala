package ca.mcit.bigdata.project1.processor

case class TypeProcessor() {
  def process(filePath: String, inputFileType: String, codeProcessor: String): Object = {
    (inputFileType.toLowerCase(), codeProcessor.toLowerCase()) match {
      case ("csv" , "trips") => return new TripsTypeProcessor.Csv().process(filePath)
      case ("csv" , "routes") => return new RoutesTypeProcessor.Csv().process(filePath)
      case ("csv" , "calendar") => return new CalendarTypeProcessor.Csv().process(filePath)
        case(a, b) => None;
    }
  }

}
