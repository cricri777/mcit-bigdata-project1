package ca.mcit.bigdata.project1.processor

import ca.mcit.bigdata.project1.model.Calendar

import scala.collection.mutable.ListBuffer

trait CalendarTypeProcessor {
  def process(filePath: String): List[Calendar]
}

object CalendarTypeProcessor {

  class Csv extends CalendarTypeProcessor {
    override def process(filePath: String): List[Calendar] = {
      var bufferedSource = io.Source.fromFile(filePath)

      var listCalendar = new ListBuffer[Calendar]()
      for (line <- bufferedSource.getLines.toStream) {
        var cols = line.split(",", -1)
        listCalendar += new Calendar(cols(0), cols(1), cols(2))
      }
      bufferedSource.close
      return listCalendar.result()
    }
  }

}
