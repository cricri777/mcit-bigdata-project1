package ca.mcit.bigdata.project1.processor

import ca.mcit.bigdata.project1.model.Routes

import scala.collection.mutable.ListBuffer


trait RoutesTypeProcessor {
  def process(filePath: String): List[Routes]
}

object RoutesTypeProcessor {
  class Csv extends RoutesTypeProcessor {
    override def process(filePath: String): List[Routes] = {
      var bufferedSource = io.Source.fromFile(filePath)

      var routes = new ListBuffer[Routes]()
      for (line <- bufferedSource.getLines.toStream) {
        var cols = line.split(",", -1)
        routes += new Routes(cols(0),cols(1),cols(2),cols(3),cols(4),cols(5),cols(6),cols(7))
      }
      bufferedSource.close
      return routes.result()
    }
  }
}
