package ca.mcit.bigdata.project1.model

case class Calendar(service_id: String,
                    date: String,
                    exception_type: String)
