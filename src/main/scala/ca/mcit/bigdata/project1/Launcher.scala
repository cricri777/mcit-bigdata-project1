package ca.mcit.bigdata.project1

import ca.mcit.bigdata.project1.model.{Calendar, Routes, Trips}
import ca.mcit.bigdata.project1.processor.TypeProcessor

object Launcher extends App {

  /**
    * File name is given by command line using option --file-name.
    * For example, --file-name=my-file.csv if the type is set to "csv" in configuration file
    */
  private val cmdLineOptions = new CommandLineOptionParser(args).parseOptions

  private val TypeProcessor = new TypeProcessor()

  private val conf = new Configuration()


  var listRoutes: List[Routes] = TypeProcessor.process(cmdLineOptions.routes, conf.inputFileType, "Routes").asInstanceOf[List[Routes]]
  var listTrips: List[Trips] = TypeProcessor.process(cmdLineOptions.trips, conf.inputFileType, "Trips").asInstanceOf[List[Trips]]
  var listCalendar: List[Calendar] = TypeProcessor.process(cmdLineOptions.calendar, conf.inputFileType, "Calendar").asInstanceOf[List[Calendar]]

  var tripsListPairedForRoutes: List[(String, Trips)] =
    listTrips.map(trip => trip.route_id -> trip)
  var routesListPaired: List[(String, Routes)] =
    listRoutes.map(route => route.route_id -> route)
  var tripsRoutesListPaired: List[(Trips, Routes)] = tripsListPairedForRoutes.map(_._2) zip routesListPaired.map(_._2)

  var tripsListPairedForCalendar: List[(String, Trips)] =
    listTrips.map(trip => trip.service_id -> trip)
  var calendarListPaired: List[(String, Calendar)] =
    listCalendar.map(calendar => calendar.service_id -> calendar)
  var tripsCalendarListPaired: List[(Trips, Calendar)] = tripsListPairedForCalendar.map(_._2) zip calendarListPaired.map(_._2)


  //result : tripsCalendarListPaired
  //result : tripsRoutesListPaired

  // Note zip function didn't worked, it was inspired by https://stackoverflow.com/questions/31465137/combine-two-lists-with-same-keys

}


